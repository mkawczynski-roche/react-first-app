const webpack = require('webpack');
const webpackHotMiddleware = require('webpack-hot-middleware');
const express = require('express');
const argv = require('yargs').argv;
const path = require('path');
const proxy = require('express-http-proxy');

const NODE_PORT = argv.port || process.env.NODE_PORT || '8080';
const NODE_HOST = argv.host || process.env.NODE_HOST || 'localhost';
const SERVER_URL = `http://${NODE_HOST}:${NODE_PORT}`;
const BUILD_DIR = path.join(__dirname, 'build');

const config = require('./webpack.config')({
  url: SERVER_URL
});
const compiler = webpack(config);
const app = express();

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath,
  stats: {
    colors: true
  }
}));
app.use(webpackHotMiddleware(compiler));

app.use('/*', (request, response) => response.sendFile(path.join(BUILD_DIR, 'index.html')));

app.listen(NODE_PORT, err => err
          ? console.error(err)
          : console.log(`Dev server started : ${SERVER_URL}`)
);
