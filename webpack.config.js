const {resolve} = require('path');
const webpack = require('webpack');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');

const config = ({url, buildDir}) => ({
        devtool: 'cheap-module-eval-source-map',

        target: 'web',
        mode: 'development',

        entry: [
            'webpack-hot-middleware/client',
            'webpack/hot/dev-server',
            './index.js'
        ],

        output: {
            filename: 'index.js',
            path: buildDir,
            publicPath: '/js/'
        },

        context: resolve(__dirname, 'src'),

        module: {
            rules: [
                {
                    test: /\.js$/,
                    loaders: [
                        'babel-loader'
                    ],
                    exclude: /node_modules/
                }
            ]
        },

        plugins: [
            new OpenBrowserPlugin({url, browser: 'chrome'}),
            new webpack.HotModuleReplacementPlugin()
        ]
    });

module.exports = config;
