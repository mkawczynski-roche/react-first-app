import {Component} from 'react';

import Todo from './Todo';


export default class TodoList extends Component {
  render() {
    const {todos} = this.props;
    return (
            <div>
                {todos.map(todo => (<Todo key={todo.id} 
                                          name={todo.name}
                                          checked={todo.checked}
                                          />))}
            </div>
            );
  }
}