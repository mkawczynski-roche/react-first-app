import React from 'react';
import ReactDOM from 'react-dom';

import TodoList from './TodoList';


window.React = React;
window.ReactDOM = ReactDOM;

const myTodos = [
  {id: 1, name: 'create new component', checked: false},
  {id: 2, name: 'add new documentation', checked: false}
];

const render = (todos = myTodos) => ReactDOM.render(
          (
                  <section className="my-app">
                      <header>My fihhrst react app</header>
                      <article>
                          <TodoList todos={todos}/>
                      </article>
                  </section>
                  ),
          document.getElementById('root')
          );


render();

//$.get('/mytodos').then(todos => {
//  render(todos);
//});

//if (module.hot) {
//  module.hot.accept('./app/app', () => {
//    const newApp = require('./app/app').default;
//    render(newApp);
//  });
//}
